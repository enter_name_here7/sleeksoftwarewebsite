<?php

/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 5/19/2016
 * Time: 11:19 AM
 */
class DataMySQL extends DataBase
{
    public function native_open() {
        $this->dbh = mysqli_connect($this->hostName, $this->username, $this->password, $this->databaseName);
        mysqli_autocommit($this->dbh, FALSE);
    }

    public function native_close() {
        mysqli_close($this->dbh);
    }

    public function native_exec() {
        $params = func_get_args();

        foreach($params as $pid => $param) {
            if ($pid != 0) {
               $params[0] = preg_replace('/[\\?]/', "'".$this->dbh->real_escape_string($params[$pid])."'" , $params[0], 1);
            }
        }

        $preparedQuery = @mysqli_prepare($this->dbh, $params[0]);
        if (!empty($preparedQuery)) {
            $preparedQuery->execute();
           $error = $preparedQuery->error;
        } else {
           $error = new DataError("999", "Failed to prepare MySQL exec statement ".$params[0]);
        }
        return $error;
    }

    public function native_error() {
        return (new DataError( mysqli_errno($this->dbh), mysqli_error($this->dbh)))->getError();
    }

    public function native_getLastId() {
        return mysqli_insert_id($this->dbh);
    }

    public function native_fetch($sql="", $noOfRecords=10, $offSet=0) {
        //Limiter for MySQL statement
        $limit ='';
        if ( strpos($sql, "show") === false ) {
            $limit = " limit {$noOfRecords} offset {$offSet} ";
        }
        $sql = $sql.$limit;

        $recordCursor = mysqli_query($this->dbh, $sql);

        $records = [];
        if ( $recordCursor === false ) {
            return $records;
        }

        for ($i = 0; $i < $noOfRecords; $i++ ) {
            $records[] = (new DataRecord( mysqli_fetch_assoc($recordCursor) ));
        }

        //populate the fields
        $fid = 0;
        $fieldInfo = $recordCursor->fetch_fields();

        foreach ($fieldInfo as $field) {
            $fields[] = (new DataField($fid, $field->name, $field->name, $field->type, $field->length));
            $fid++;
        }

        return (new DataResult($records, $fields, $noOfRecords, $offSet));
    }

    public function native_commit() {
        @mysqli_commit($this->dbh);
    }

    public function native_rollback()
    {
        @mysqli_rollback($this->dbh);
    }
}