<?php
/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 2016/02/09
 * Time: 02:46 PM
 * Purpose: Stub to serve PHP files from the PHAR file
 */

//defines
define("TINA4_POST" , "POST");
define("TINA4_GET"  , "GET");
define("TINA4_ANY"  , "ANY");
define("TINA4_PUT"  , "PUT");
define("TINA4_PATCH"  , "PATCH");
define("TINA4_DELETE"  , "DELETE");

require_once "PHPLinq.php";
require_once "phpFastCache/CacheManager.php";


function redirect ($path="/") {
    $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . $path;
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

function tina4_autoloader($class) {
    $root = dirname(__FILE__);

    if (file_exists("{$root}/".str_replace("_","/",$class). ".php")) {
        include "{$root}/" . str_replace("_", "/", $class) . ".php";
    }  else {
        if (defined("TINA4_INCLUDE_LOCATIONS")) {
            foreach (TINA4_INCLUDE_LOCATIONS as $lid => $location) {
                if (file_exists($_SERVER["DOCUMENT_ROOT"]."/{$location}/{$class}.php")) {
                    require_once $_SERVER["DOCUMENT_ROOT"] . "/{$location}/{$class}.php";
                    break;
                }
            }
        }
    }

}

function __autoload($class)
{
    tina4_autoloader ($class);
    spl_autoload_register('tina4_autoloader');
    if (file_exists($_SERVER["DOCUMENT_ROOT"]."/vendor/autoload.php")) {
        require_once $_SERVER["DOCUMENT_ROOT"]."/vendor/autoload.php";
    }
}
__halt_compiler();