<?php
/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 2016/02/09
 * Time: 02:02 PM
 * Purpose: Determine routing responses and data outputs based on the URL sent from the system
 * You are welcome to read and modify this code but it should not be broken, if you find something to improve it, send me an email with the patch
 * andrevanzuydam@gmail.com
 */

class Routing
{
    private $session;
    private $request;
    private $parser;
    private $params;
    private $content;
    private $debug = false;
    private $method;
    private $pathMatchExpression = "/([a-zA-Z0-9\\ \\! \\-\\}\\{\\.]*)\\//";

    function debug($msg)
    {
        if ($this->debug) {
            echo date("\nY-m-d h:i:s - ") . $msg . "\n";
        }
    }

    function matchPath($path, $routePath)
    {
        $this->debug("Matching {$path} with {$routePath}");
        if ($routePath !== "/") $routePath .= "/";
        preg_match_all($this->pathMatchExpression, $path, $matchesPath);
        preg_match_all($this->pathMatchExpression, $routePath, $matchesRoute);

        if (count($matchesPath[1]) == count($matchesRoute[1])) {
            $matching = true;
            $variables = [];

            foreach ($matchesPath[1] as $rid => $matchPath) {
                if (!empty($matchesRoute[1][$rid]) && strpos($matchesRoute[1][$rid], "{") !== false) {
                    $variables[] = $matchPath;
                } else
                    if (!empty($matchesRoute[1][$rid])) {

                        if ($matchPath !== $matchesRoute[1][$rid]) {
                            $matching = false;
                        }
                    } else

                        if (empty($matchesRoute[1][$rid]) && $rid !== 0) {
                            $matching = false;
                        }
            }

        } else {
            $matching = false; //The path was totally different from the route
        }

        if ($matching) {
            $this->params = $variables;
            $this->debug("Found match {$path} with {$routePath}");
        } else {
            $this->debug("No match for {$path} with {$routePath}");
        }
        return $matching;
    }

    function getParams()
    {
        return $this->params;
    }

    function cleanURL($url) {
        $url = explode("?", $url, 2);
        return $url[0];
    }

    function __construct($root = "", $urlToParse = "", $method = "")
    {
        global $arrRoutes;

        //Initialize debugging
        if ($this->debug) {
            echo "<PRE>";
        }

        //Generate a filename just in case the routing doesn't find anything
        if ($urlToParse === "/") {
            $fileName = "index.html";
        } else {
            $ext = pathinfo($urlToParse, PATHINFO_EXTENSION);
            if (empty($ext)) {
                $fileName = $urlToParse . ".html";
                $fileName = str_replace("/.html", "/index.html", $fileName);
            } else {
                $fileName = $urlToParse;
            }
        }

        $urlToParse = $this->cleanURL($urlToParse);

        if ($urlToParse !== "/") {
            $urlToParse .= "/";
            $urlToParse = str_replace("//", "/", $urlToParse);
        }

        $this->content = "";
        $this->method = $method;
        $this->debug("Root: {$root}");
        $this->debug("URL: {$urlToParse}");
        $this->debug("Method: {$method}");

        //include routes in routes folder
        foreach (TINA4_ROUTE_LOCATIONS as $rid => $route) {
            $d = dir($root . "/" . $route);
            while (($file = $d->read()) !== false) {
                if ($file != "." && $file != "..") {
                    $fileNameRoute = realpath(getcwd() . "/web_root/" . $route) . "/" . $file;
                    require_once $fileNameRoute;
                }
            }
            $d->close();
        }

        //determine what should be outputted and if the route is found
        $matched = false;

        if ($this->debug) {
            print_r ($arrRoutes);
        }
        //iterate through the routes
        foreach ($arrRoutes as $rid => $route) {
            $result = "";
            if ($this->matchPath($urlToParse, $route["routePath"]) && ($route["method"] === $this->method || $route["method"] == TINA4_ANY)) {
                //call closure with & without params
                $reflection = new ReflectionFunction($route["function"]);
                $arguments = $reflection->getParameters();
                if (!empty($arguments)) {
                    $result = call_user_func_array($route["function"], $this->getParams($route["routePath"]));
                } //call with params
                else {
                    $result = call_user_func($route["function"]);
                }
                $matched = true;
                break;
            }
        }

        //result was empty we can parse for templates
        if (!$matched) {
            //if there is no file passed, go for the default or make one up
            if (empty($fileName)) {
                $fileName = "index.html";
            }
            
            $this->content .= new Parser($root, $fileName);
        }

        //end debugging
        if ($this->debug) {
            echo "</PRE>";
        }
    }

    //convert the output to a string value
    function __toString()
    {
        return $this->content;
    }

}