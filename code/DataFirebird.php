<?php

/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 5/19/2016
 * Time: 11:19 AM
 */
class DataFirebird extends DataBase
{
    public function native_open() {
        $this->dbh = ibase_connect($this->hostName.":".$this->databaseName, $this->username, $this->password); //create the new database or open existing one
    }

    public function native_close() {
        ibase_close($this->dbh);
    }

    public function native_exec() {
        $params = func_get_args();
        $preparedQuery = @ibase_prepare($params[0]);
        if (!empty($preparedQuery)) {
            $params[0] = $preparedQuery;
            @call_user_func_array("ibase_execute", $params);
        }
        return $this->error();
    }

    public function native_error() {
        return (new DataError( ibase_errcode(), ibase_errmsg()))->getError();
    }

    public function native_fetch($sql="", $noOfRecords=10, $offSet=0) {
        $limit = " first {$noOfRecords} skip {$offSet} ";

        $ipos = stripos($sql, "select")+strlen("select");

        $sql = substr($sql,0, $ipos).$limit.substr($sql,$ipos);

        $recordCursor = ibase_query($this->dbh, $sql );

        $records = [];

        for ($i = 0; $i < $noOfRecords; $i++ ) {
            $records[] = (new DataRecord( ibase_fetch_assoc($recordCursor) ));
        }

        //populate the fields
        $fid = 0;
        $fields = [];
        foreach ($records[0] as $field => $value) {
            $fieldInfo = ibase_field_info($recordCursor, $fid);

            $fields[] = (new DataField($fid, $fieldInfo["name"], $fieldInfo["alias"], $fieldInfo["type"], $fieldInfo["length"]));
            $fid++;
        }

        return (new DataResult($records, $fields, $noOfRecords, $offSet));
    }

    public function native_commit() {
        //No commit for sqlite
        @ibase_commit($this->dbh);
    }
}