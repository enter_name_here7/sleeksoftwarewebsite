<?php

/**
 * Created by PhpStorm.
 * User: S946115
 * Date: 2016/03/01
 * Time: 04:30 PM
 * Note: A result from the data is a collection of records which can be in an OBJECT form or Key Value form
 */
class DataResult implements Iterator
{

    private $position = 0;


    private $records;
    private $fields;
    private $noOfRecords;
    private $offSet;

    function __construct($records, $fields, $noOfRecords, $offSet = 0)
    {
        $this->records = $records;
        $this->fields = $fields;
        $this->noOfRecords = $noOfRecords;
        $this->offSet = $offSet;
        $this->position = 0;
    }

    function record($id = 0)
    {
        return $this->records[$id];
    }

    function rewind()
    {
        $this->position = 0;
    }

    function current()
    {
        return $this->records[$this->position];
    }

    function key()
    {
        return $this->position;
    }

    function next()
    {
        ++$this->position;
    }

    function valid()
    {
        if ($this->position < count($this->records) - 1) {
            return !empty($this->records[$this->position]);
        } else {
            return false;
        }
    }

    function toDataTables()
    {
        $result = [];
        foreach ($this->records as $rid => $record) {
            $recordArray = [];
            if (!empty($record->asArray())) {
                foreach ($record->asArray() as $column => $value) {
                    $recordArray[$column] = $value;
                }

                $result[] = $recordArray;
            }
        }

        return json_encode($result);
    }

    function __toString()
    {
        $result = [];
        foreach ($this->records as $rid => $record) {
            $result[] = $record->asObject();
        }

        return json_encode($result);
    }

    function getFields()
    {
        return $this->fields;
    }

}