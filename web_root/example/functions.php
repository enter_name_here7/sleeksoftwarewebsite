<?php
/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 19/07/2016
 * Time: 1:44 PM
 */

$MYSQL_server = "127.0.0.1";
$MYSQL_username = "root";
$MYSQL_password = "masterkey";
$MYSQL_db = "test2";


$conn = mysqli_connect($MYSQL_server, $MYSQL_username, $MYSQL_password);

$sqlCreate = "create database {$MYSQL_db}";

if (mysqli_query($conn, $sqlCreate)) {
    //database created
    mysqli_select_db($conn, $MYSQL_db);
    //create the tables
    $sqlCreateTable = "
        create table clients (
          clientID integer auto_increment,
          name varchar(255) default null,
          idNo varchar(32) default null,
          email varchar(255) default null,
          tel varchar(32) default null,
          cell varchar(32) default null,
          premiumsCollected decimal(16,2) default null,
          claimsTotalAmount decimal(16,2) default null,
          primary key (clientID)
        )

    ";

    mysqli_query($conn, $sqlCreateTable);

} else {
    //database exists
    mysqli_select_db($conn, $MYSQL_db);
}


