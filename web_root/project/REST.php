<?php

/**
 * Created by PhpStorm.
 * User: User-PC
 * Date: 23/08/2016
 * Time: 9:55 AM
 */
class REST extends Sleek
{
    /**
     * method to get information from database
     */
    function getTableData($tableName, $request)
    {
        $filter = ""; //for the where statement
        $orderBy = ""; //for the order by statement
        $tableHtml = "";//html for creating Table
        $tableScript = "";//script for table

        if (!empty($request["order"])) {
            $orderBy = "order by ";
            foreach ($request["order"] as $oid => $orderColumn) {
                $orderBy .= ($orderColumn["column"] + 1) . " " . $orderColumn["dir"];
            }

        }

        if (!empty($request["columns"])) {
            foreach ($request["columns"] as $cId => $column) {
                if ($column["searchable"]) {
                    if (empty($filter)) {
                        $filter = "where (";
                    } else {
                        $filter .= " or ";
                    }
                    $filter .= "upper(trim(" . strtolower($column["data"]) . ")) like upper('" . $request["search"]["value"] . "%')"; // glcode like 'water%'
                }
            }
            $filter .= ")";
        }

        if (empty($request["length"])) {
            $request["length"] = 10;
        }

        if (empty($request["start"])) {
            $request["start"] = 0;
        }

        if (empty($request["draw"])) {
            $request["draw"] = 0;
        }

        $sql = "select * from {$tableName} {$filter} {$orderBy}";

        if (!empty($request["generate"])) {

            $fields = $this->DB->fetch("select * from {$tableName}", 1)->getFields();
            foreach ($fields as $fId => $field) {
                $columns[] = ["data" => $field->fieldName];
                if($fId != 0) {
                    $tableHead [] ="\t" . "\t" . "\t" . "\t". "<th>" . "$field->fieldName" . "</th>";
                }else{
                    $tableHead [] = "<th>" . "$field->fieldName" . "</th>";
                }
            }
            $columnName = "columns:" . json_encode($columns);

            $tableHtml = "
            <html>
                <title>{$tableName}</title>
                <body>
                    <!--display page header also include css an jquery-->
                    <!--{{include:includes/header}}-->
                    <table id=\"{$tableName}\" class=\"display\" cellspacing=\"0\" width=\"100%\">
                        <thead>
                            <tr>
                                " . join($tableHead, "\r\n") . "
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                " . join($tableHead, "\r\n") . "
                            </tr>
                        </tfoot>
                    </table>
                    <!--display the page footer-->
                    <!--{{include:includes/footer}}-->
                </body>
            </html>
               ";

            $tableJson = ("
                    $('#{$tableName}').dataTable({
                        \"processing\": true,
                        \"serverSide\": true,
                        \"ajax\": {
                            \"url\": \"/data/{$tableName}\",
                             \"type\": \"GET\"
                         },
                        {$columnName}
                    });
            ");

            $tableScript = "
            <script>
                $(document).ready(function () {
                " . ($tableJson) . " });  
            </script>";

            return "
            Copy the code below to generate your table
            <pre style='background-color: #00b3ee'>" .htmlentities(  $tableHtml . $tableScript ). "</pre>
             ";

        } else {
            $countResult = $this->DB->fetch("select count(*) as count_records from {$tableName} {$filter}");

            return "{\"SQL\": \"{$sql}\",\"draw\":{$request["draw"]},\"recordsTotal\":{$request["length"]},\"recordsFiltered\":" . $countResult->record(0)->byName("COUNT_RECORDS") . ",  \"data\":" . $this->DB->fetch($sql, $request["length"], $request["start"])->toDataTables() . "}";
        }
    }

}