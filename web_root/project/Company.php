<?php

/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 5/26/2016
 * Time: 12:08 PM
 */
class Company extends Sleek
{
    function getCompanyTypes() {
        $companyTypes = $this->DB->fetch("select * from comtype where comtype <> '000'");

        $html = "";
        $default = "";
        foreach ($companyTypes as $cid => $companyType) {
            if ($companyType->COMTYPE === "PTY") {
                $default = "selected";
            } else {
                $default = "";
            }
            $html .= "<option {$default} value={$companyType->COMTYPE}>{$companyType->DESCRIPTION}</option>";
        }

        return $html;
    }

}