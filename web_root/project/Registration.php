<?php

/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 5/16/2016
 * Time: 3:03 PM
 */
class Registration extends Sleek
{
    private $name="Test!";

    function get($value) {
        return "get method {$value}";
    }

    function __construct($name="testing")
    {
        $this->name = $name;
    }


    function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->name;
    }

}