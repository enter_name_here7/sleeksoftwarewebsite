<?php
/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 5/18/2016
 * Time: 1:57 AM
 */

Post::add("/google_auth",
    function() {
        unset($_SESSION["access_token"]);
        $client = new Google_Client();
        $client->setAuthConfigFile($_SERVER["DOCUMENT_ROOT"].'/assets/api_secret.json');
        $client->addScope(Google_Service_Plus::USERINFO_EMAIL);
        $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/auth';
        redirect($redirect_uri);
    }
);


Post::add("/register",
    function() {

        $confirmCode = md5($_REQUEST["email"].$_REQUEST["password"]);
        $link =  "http://".$_SERVER["HTTP_HOST"]. "/confirm/{$confirmCode}";
        $email = $_REQUEST["email"];
        
        //add the user
        
        $error = (new User())->add($email, '', 0, $confirmCode);


        $welcomeMessage = new Parser($_SERVER["DOCUMENT_ROOT"], "templates/welcome", get_defined_vars());

        $error = (new Emailer())->sendMail($email, "Sleek Accounting - Confirm email address" , $welcomeMessage , "Sleek Accounting" , "no-reply@sleekacc.co.za");

        redirect("/thank_you");
    }
);

Post::add("/register_company",
    function() {
        $_SESSION["company"] = array("id");
        redirect("/dashboard");
    }
);


Get::add ("/thank_you",
    function () {
        echo new Parser( $_SERVER["DOCUMENT_ROOT"], "thank_you" );
    }
);



Get::add("/cancel", function() {
   unset($_SESSION);
   redirect("/");
});



Get::add ("/confirm/{confirmationCode}",
    function($confirmationCode) {
        //http://localhost:11445/confirm/8ad9ca28a97aa3b3b9dc4b7c651255b7
        if (!empty($_SESSION["company"])) {
            unset($_SESSION["company"]);
        }
        $user = (new User())->confirm($confirmationCode);

        if (!empty($user)) {
            $_SESSION["user"] = $user;
            redirect("/dashboard");
        }
    }

);

Get::add("/auth",
  function() {
      $client = new Google_Client();
      $client->setAuthConfigFile($_SERVER["DOCUMENT_ROOT"].'/assets/api_secret.json');
      $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/auth');
      $client->addScope(Google_Service_Plus::USERINFO_EMAIL);


      if (! isset($_GET['code'])) {
          $auth_url = $client->createAuthUrl();
          header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
      } else {
          $client->authenticate($_GET['code']);
          $_SESSION['access_token'] = $client->getAccessToken();
          $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/dashboard';
          header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
      }

  }
);