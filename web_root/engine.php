<?php
/**
 * Created by PhpStorm.
 * User: Andre van Zuydam
 * Date: 2016/02/09
 * Time: 01:54 PM
 * Purpose: This is th engine room of the stack which glues everything together and returns results, Tina4Stackv2 will work on a "routerless" concept
 * where files stored under assets will be served and parsed.
 * You are welcome to read and modify this code but it should not be broken, if you find something to improve it, send me an email with the patch
 * andrevanzuydam@gmail.com
 */

//root of the system
global $root;
$root = dirname(__FILE__);


/**
 * Global array to store the routes that are declared during runtime
 */
global $arrRoutes;
$arrRoutes = [];



//include build
include("{$root}/../build.php");

//system defines, perhaps can be defined or overridden by a config.php file.
if (file_exists("{$root}/config.php")) {
    require_once ($root."/config.php");
}


if(!defined("TINA4_TEMPLATE_LOCATIONS")) {
    define("TINA4_TEMPLATE_LOCATIONS" , ["assets", "assets/pages"]);
}
if(!defined("TINA4_ROUTE_LOCATIONS")) {
    define("TINA4_ROUTE_LOCATIONS"    , ["routes"]);
}
if(!defined("TINA4_INCLUDE_LOCATIONS")) {
    define("TINA4_INCLUDE_LOCATIONS"  , ["project", "objects"]);
}
if(!defined("TINA4_CACHE_CONFIG")) {
    define("TINA4_CACHE_CONFIG"  , ["storage"   =>  "files", "path"      =>  $root."/cache"]);
}

if(!defined("TINA4_SESSION_NAME")) {
    define("TINA4_SESSION_NAME"  , "tina4Session");
}

if (!defined("TINA4_USE_CACHE")) {
    define("TINA4_USE_CACHE", false);
}


//include the library for database engine, routing & template system
include $root."/core/tina4stackv2.phar";

//start the session
session_name(TINA4_SESSION_NAME);
ini_set('session.save_path',realpath(dirname($_SERVER['DOCUMENT_ROOT']) . '/../session'));
session_start();



global $cache;
use phpFastCache\CacheManager;

if (TINA4_USE_CACHE) {
    //See if cache directory exists
    if (!file_exists($root . "/cache")) {
        mkdir($root . "/cache");
    }

    CacheManager::setup(TINA4_CACHE_CONFIG);
    CacheManager::CachingMethod("phpfastcache"); // use "normal" as traditional

    $cache = CacheManager::getInstance();
} else {
    $cache = null;
    ini_set('opcache.enable', '0');
}

//check for static files that need to be served
if ($_SERVER["REQUEST_URI"] !== "/" && file_exists($_SERVER["DOCUMENT_ROOT"] . $_SERVER["REQUEST_URI"])) {
    return false;
} else {
    //do the routing and come up with a result, this should be the last line of code regardless
    echo new Routing($root, $_SERVER["REQUEST_URI"], $_SERVER["REQUEST_METHOD"]);
}