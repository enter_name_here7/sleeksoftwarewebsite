<!--display page header also include css an jquery-->
{{include:includes/header}}
<table id="glcode" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>GLCODE</th>
        <th>DESCRIPTION</th>
        <th>GLGROUP</th>
        <th>MANCODE</th>
        <th>RECCODE</th>
        <th>CONTROL</th>
        <th>TAXCODE</th>
        <th>ISCASHBOOK</th>
        <th>GROUPTYPE</th>
        <th>DATECRT</th>
        <th>DATEMOD</th>
        <th>FACODE</th>
        <th>COSTCODE</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
    <tr>
        <th>GLCODE</th>
        <th>DESCRIPTION</th>
        <th>GLGROUP</th>
        <th>MANCODE</th>
        <th>RECCODE</th>
        <th>CONTROL</th>
        <th>TAXCODE</th>
        <th>ISCASHBOOK</th>
        <th>GROUPTYPE</th>
        <th>DATECRT</th>
        <th>DATEMOD</th>
        <th>FACODE</th>
        <th>COSTCODE</th>
    </tr>
    </tfoot>
</table>
<!--display the page footer-->
<!--{{include:includes/footer}}-->

{{User::getName()}}
<script>
    $(document).ready(function () {

        $('#glcode').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/data/glcode",
                "type": "GET"
            },
            columns:[{"data":"GLCODE"},{"data":"DESCRIPTION"},{"data":"GLGROUP"},{"data":"MANCODE"},{"data":"RECCODE"},{"data":"CONTROL"},{"data":"TAXCODE"},{"data":"ISCASHBOOK"},{"data":"GROUPTYPE"},{"data":"DATECRT"},{"data":"DATEMOD"},{"data":"FACODE"},{"data":"COSTCODE"}]
        });
    });
</script>